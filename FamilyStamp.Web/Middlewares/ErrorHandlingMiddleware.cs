﻿using log4net;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FamilyStamp.Web.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        static ILog logger = LogManager.GetLogger(typeof(ErrorHandlingMiddleware));

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            logger.Error(context.Request.Path, ex);

            bool apiException = Regex.IsMatch(context.Request.Path.Value, "^/api/", RegexOptions.IgnoreCase);
            if (apiException)
            {
                string message = "系統忙碌中，請稍後再試";
                if (ex is Core.Exceptions.FamilyStampApiException)
                {
                    message = ex.Message;
                }
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var json = new Core.ModelCustom.API.ApiResult
                {
                    Code = Core.Enums.ApiResultCode.Error,
                    Message = message
                };
                context.Response.WriteAsync(new Core.Components.JsonSerializer().Serialize(json));
            }
            else
            {
                context.Response.ContentType = "text/html";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                string errorHtml = string.Format(@"
<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'
  </head>
  <body><h2>{0}</h2>
  </body>
</html>
", ex.Message);
                //OopsClient.Instance.Alert(ex, context);
                return context.Response.WriteAsync(errorHtml);
            }

            return context.Response.WriteAsync(string.Empty);
        }
    }
}
