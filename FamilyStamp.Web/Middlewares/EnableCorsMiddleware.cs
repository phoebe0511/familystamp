﻿using log4net;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FamilyStamp.Web.Middlewares
{
    public class EnableCorsMiddleware
    {
        ILog logger = LogManager.GetLogger(typeof(EnableCorsMiddleware));
        private readonly RequestDelegate _next;

        public EnableCorsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext context)
        {
            return BeginInvoke(context);
        }

        private Task BeginInvoke(HttpContext context)
        {
            context.Response.Headers.Add("Access-Control-Allow-Origin", new[] {
                    (string)context.Request.Headers["Origin"] });
            context.Response.Headers.Add("Access-Control-Allow-Headers",
                new[] { (string)context.Request.Headers["Access-Control-Request-Headers"] });
            context.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET, POST, PUT, DELETE, OPTIONS" });
            context.Response.Headers.Add("Access-Control-Allow-Credentials", new[] { "true" });

            if (context.Request.Method == "OPTIONS")
            {
                context.Response.ContentType = "text/plain";
                context.Response.StatusCode = 200;
                return context.Response.WriteAsync("OK");
            }

            context.Response.Cookies.Append(
                "CookieKey",
                "CookieValue",
                new CookieOptions
                {
                    HttpOnly = true
                });

            return _next.Invoke(context);
        }
    }
}
