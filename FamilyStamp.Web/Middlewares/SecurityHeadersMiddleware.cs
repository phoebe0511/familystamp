﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FamilyStamp.Web.Middlewares
{
    public class SecurityHeadersMiddleware
    {
        private readonly RequestDelegate _next;

        public SecurityHeadersMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            IHeaderDictionary headers = context.Response.Headers;
            headers.Remove("X-Powered-By");
            headers.Add("X-Frame-Options", "SAMEORIGIN");
            headers.Add("X-XSS-Protection", "1; mode=block");
            headers.Add("X-Content-Type-Options", "nosniff");
            await _next(context);
        }
    }
}
