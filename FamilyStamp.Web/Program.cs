using FamilyStamp.Core.Components.Jobs;
using FamilyStamp.Core.Kernel;
using FamilyStamp.Core.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Reflection;

namespace FamilyStamp
{
    public class Program
    {
        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(typeof(Program));
        private static readonly ISystemConfig _config = Ioc.GetConfig();
        public static void Main(string[] args)
        {
            InitLog4net();
            Ioc.Register(args);
            JobManager.Current.Start();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel(options =>
                    {
                        options.Limits.MaxConcurrentConnections = 1000;
                        //options.ListenAnyIP(Ioc.GetConfig().Port);
                        options.AddServerHeader = false;
                    });
                    webBuilder.UseIISIntegration();
                    webBuilder.UseIIS();
                    webBuilder.UseStartup<Startup>();
                });

        static void InitLog4net()
        {
            var repo = log4net.LogManager.CreateRepository(
                Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            string logConfigFilePath = Helper.MapPath("log4net.config");
            FileInfo fi = new FileInfo(logConfigFilePath);
            if (fi.Exists)
            {
                log4net.Config.XmlConfigurator.Configure(repo, fi);
                //logger.Debug("Application - Main is invoked");
            }
        }
    }
}
