﻿var pagginationComponent = {
    template: `
<div v-show="pageArray.length>0">
    <ul class="pagination">
        <li class="page-item disabled"><a class="page-link" href="javascript:void(0)">共{{pageSize}}頁/每頁{{pageLength}}筆</a></li>
        <div v-if="currentPage==1">
            <li class="page-item disabled"><a class="page-link" href="javascript:void(0)">上一頁</a></li>
        </div>
        <div v-else>
            <li class="page-item"><a class="page-link" href="javascript:void(0)" v-on:click="loadData(currentPage-1)">上一頁</a></li>
        </div>

        <div style="line-height:30px;" v-if="morePrev">
            <div>
                <li class="page-item disabled">
                    <a href="javascript:void(0)" class="page-link" >
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                    </a>
                </li>
            </div>
        </div>

        <div v-for="(item,index) in pageArray">
            <div>
                <li class="page-item" v-bind:class="{ active: item === currentPage }"><a class="page-link" href="javascript:void(0)" v-on:click="loadData(item)">{{item}}</a></li>
            </div>
        </div>


        <div v-if="morePage">
            <div>
                <li class="page-item disabled">
                    <a href="javascript:void(0)" class="page-link" >
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                    </a>
                </li>
            </div>
        </div>

        <div v-if="currentPage==pageSize">
            <li class="page-item disabled"><a class="page-link" href="javascript:void(0)">下一頁</a></li>
        </div>
        <div v-else>
            <li class="page-item"><a class="page-link" href="javascript:void(0)" v-on:click="loadData(currentPage+1)">下一頁</a></li>
        </div>
    </ul>
</div>	
            `,
    name: 'uploadImage',
    data: function () {
        return {
            morePage: false,
            morePrev: false
        }
    },
    props: {
        'currentPage': {
            type: Number,
            default: 1,
        },
        'pageSize': {
            type: Number,
            default: 1,
        },
        'pageLength': {
            type: Number,
            default: 10,
        },
    },
    computed: {
        pageArray: function () {
            var pages = [];
            var startPage = 1;
            this.morePrev = false;
            this.morePage = false;
            if ((this.currentPage % this.pageLength) == 0) {
                startPage = (parseInt(this.currentPage / this.pageLength) - 1) * this.pageLength + 1;
            } else {
                startPage = parseInt(this.currentPage / this.pageLength) * this.pageLength + 1;
            }
            for (var idx = startPage; idx <= this.pageSize; idx++) {
                if (pages.length >= this.pageLength) {
                    this.morePage = true;
                    return pages;
                }
                pages.push(idx);
            }
            if (startPage != 1) {
                this.morePrev = true;
            }
            return pages;
        },
    },
    methods: {
        loadData: function (item) {
            this.$emit('load-data', item);
        },
    }
};