using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using FamilyStamp.Core.ModelCustom;
using FamilyStamp.Core.Facade;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace FamilyStamp.Web.Views.Login
{
    public class IndexModel : PageModel
    {
        public IActionResult OnGet()
        {
            if (HttpContext.User.Identity.IsAuthenticated == true)
            {
                return Redirect("/home");
            }
            return null;
        }

        public async Task<IActionResult> OnPost()
        {
            //var reqCookies = Request.Cookies.ToList();
            var account = Request.Form["account"];
            var password = Request.Form["password"];
            BackstageUserModel user = null;
            if (BackstageUserFacade.VerifyUser(account, password, out user))
            {
                Claim[] claims = new[] { new Claim(ClaimTypes.Name, account), new Claim(ClaimTypes.Role, ((int)user.Role).ToString()) };

                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);
                await HttpContext.SignInAsync(principal);
                //await HttpContext.SignInAsync(principal,
                //    new AuthenticationProperties()
                //    {
                //        IsPersistent = false, //IsPersistent = false，瀏覽器關閉即刻登出
                //        //用戶頁面停留太久，逾期時間，在此設定的話會覆蓋Startup.cs裡的逾期設定
                //        //ExpiresUtc = DateTime.Now.AddMinutes(loginExpireMinute)
                //    });
                if(user.Role == Core.Enums.BackstageMemberRole.Master)
                {
                    return Redirect("/Home");
                }
                else
                {
                    return Redirect("/Activity/List");
                }
            }
            else
            {
                TempData["account"] = account;
                TempData["message"] = "帳號或密碼錯誤";
            }
            return Redirect("/Login");
        }
    }

}
