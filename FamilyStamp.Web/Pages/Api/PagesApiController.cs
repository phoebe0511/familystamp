﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FamilyStamp.Core.Attributes;
using FamilyStamp.Core.DbContenxts;
using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Kernel;
using FamilyStamp.Web.Filters;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FamilyStamp.Web.Pages.Api
{
    [PagesApiAuthorization]
    [ApiController]
    [Route("api/page/[Action]")]
    public class PagesApiController : Controller
    {
        [PagesApiAuthorization]
        [HttpPost]
        public dynamic GetEnviromentInfo()
        {
            var config = Ioc.GetConfig();
            var connsb = new SqlConnectionStringBuilder(config.connectionString);
            string name = connsb.InitialCatalog + "(" + connsb.DataSource + ")";

            object dbObj = SqlHelper.GetDbSchemaVersion();
            string currentVersion = string.Empty;
            if (dbObj != null)
            {
                currentVersion = dbObj.ToString();
            }

            var attr =
                typeof(AssemblyDatabaseSchemaAttribute).Assembly.GetCustomAttribute<AssemblyDatabaseSchemaAttribute>();
            string requiredVersion = new Version(attr.SchemaVersion).ToString();

            return new
            {
                Database = new
                {
                    Name = name,
                    CurrentVersion = currentVersion,
                    RequiredVersion = requiredVersion
                },
            };
        }

    }
}
