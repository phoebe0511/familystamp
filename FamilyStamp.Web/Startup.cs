using System;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using FamilyStamp.Web.Middlewares;
using log4net;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.WebEncoders;

namespace FamilyStamp
{
    public class Startup
    {
        private ILog _logger = LogManager.GetLogger(typeof(Startup));
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _logger.Info("Startup");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllersWithViews();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
            {
                //或許要從組態檔讀取，自己斟酌決定
                option.LoginPath = new PathString("/Login");
                option.LogoutPath = new PathString("/Logout");
                option.ExpireTimeSpan = TimeSpan.FromMinutes(60);
            });

            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.WriteIndented = false;
                    options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(new TextEncoderSettings(UnicodeRanges.All));
                });

            services.AddMvc().AddRazorPagesOptions(options =>
            {
                options.Conventions.AddPageRoute("/Login/Index", "");
            });

            ////services.AddMvc()
            ////    .AddNewtonsoftJson(options =>
            ////        options.SerializerSettings.ContractResolver = new
            ////        CamelCasePropertyNamesContractResolver())
            ////    .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNameCaseInsensitive = true);


            services.AddRazorPages();
            services.AddHttpContextAccessor();


            services.AddRazorPages()
                 .AddRazorPagesOptions(options =>
                 {
                     options.Conventions.AuthorizeFolder("/Api");
                     options.Conventions.AuthorizeFolder("/Account");
                     options.Conventions.AuthorizeFolder("/Home");
                     options.Conventions.AuthorizeFolder("/Activity");
                     // options.Conventions.AuthorizeFolder("/Product");
                     options.Conventions.AllowAnonymousToFolder("/Login");
                 });

            services.Configure<WebEncoderOptions>(options =>
                options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.BasicLatin,
                    UnicodeRanges.CjkUnifiedIdeographs));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();


            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<EnableCorsMiddleware>();
            app.UseMiddleware<SecurityHeadersMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllerRoute(
            //        name: "default",
            //        pattern: "{controller=Home}/{action=Index}/{id?}");
            //});
        }
    }
}
