﻿
using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Kernel;
using FamilyStamp.Core.ModelCustom.API;
using FamilyStamp.Core.Providers;
using FamilyStamp.Core.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace FamilyStamp.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class PagesApiAuthorizationAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        private static IBackstageUserProvider _bsp = Ioc.Get<IBackstageUserProvider>();
        public bool authorized { get; }
        private MenuItem pageId;
        public PagesApiAuthorizationAttribute(MenuItem pageId = MenuItem.Master, bool authorized = true)
        {
            this.authorized = authorized;
            this.pageId = pageId;
        }
        ConcurrentDictionary<string, bool>
            apiControllers = new ConcurrentDictionary<string, bool>();
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            bool bReault = false;
            if (context.HttpContext.User.Identity.IsAuthenticated == false)
            {
                bool allowGuest = context.ActionDescriptor.FilterDescriptors
                    .Any(t => (t.Filter is PagesApiAuthorizationAttribute)
                        && (t.Filter as PagesApiAuthorizationAttribute).authorized == false);
                if (allowGuest)
                {
                    return;
                }
            }
            else
            {
                BackstageMemberRole role = (BackstageMemberRole)int.Parse(context.HttpContext.User.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value).SingleOrDefault());
                if(this.pageId == MenuItem.Master && role == BackstageMemberRole.Master)
                {
                    bReault = true;
                }
                else if (this.pageId < MenuItem.Master)
                {
                    var pageIds = _bsp.GetMemberRoles((int)role);
                    if (pageIds.Contains((int)pageId))
                    {
                        bReault = true;
                    }
                }
            }
            if (!bReault)
            {
                context.Result = new JsonResult(new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn,
                    Message = Helper.GetEnumDescription(ApiResultCode.UserNoSignIn)
                });
            }
        }
    }
}
