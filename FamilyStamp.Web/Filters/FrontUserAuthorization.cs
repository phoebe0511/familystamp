﻿using FamilyStamp.Core.ModelCustom.API;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Security.Claims;

namespace FamilyStamp.Web.Filters
{
    public class FrontUserAuthorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            StringValues strValues;
            if (context.HttpContext.Request.Headers.TryGetValue("Authorization", out strValues) == false)
            {
                //context.Result = new StatusCodeResult((int)HttpStatusCode.Unauthorized);
                context.Result = new ObjectResult(new ApiResult
                {
                    Code = Core.Enums.ApiResultCode.TokenExpired,
                    Message = "token was expired."
                });
                return;
            }
            //FrontIdentity frontUser;
            //FrontUserTokenInfo tokenInfo;
            //int vendorId = 0;
            //if (OAuthFacade.TryParseFrontUserTokenInfo(strValues[0], out vendorId, out tokenInfo) == false)
            //{
            //    //context.Result = new StatusCodeResult((int)HttpStatusCode.Unauthorized);
            //    context.Result = new ObjectResult(new ApiResult
            //    {
            //        Code = Core.Enums.ApiResultCode.TokenInvalid,
            //        Message = "token was expired."
            //    });
            //    return;
            //}
            //if(tokenInfo == null)
            //{
            //    context.Result = new ObjectResult(new ApiResult
            //    {
            //        Code = Core.Enums.ApiResultCode.TokenInvalid,
            //        Message = "token was expired."
            //    });
            //    return;
            //}
            //if (tokenInfo.expiredtime < DateTime.Now)
            //{
            //    context.Result = new ObjectResult(new ApiResult
            //    {
            //        Code = Core.Enums.ApiResultCode.TokenExpired,
            //        Message = "token_info was expired."
            //    });
            //    return;
            //}

            //frontUser = new FrontIdentity(vendorId.ToString())
            //{
            //    TokenId = tokenInfo.tokenid,
            //    Name = tokenInfo.merchantId.ToString(),
            //    MerchantApiFormat = tokenInfo.merchantFormat
            //};


            //var user = new ClaimsPrincipal();
            //user.AddIdentity(frontUser);

            //context.HttpContext.User = user;

            base.OnActionExecuting(context);
        }
    }
}
