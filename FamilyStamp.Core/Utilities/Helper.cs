﻿using Microsoft.Extensions.Primitives;
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace FamilyStamp.Core.Utilities
{
    public class Helper
    {
        private static string rootPath;

        public static string MapPath(params string[] paths)
        {
            if (paths.Length == 1)
            {
                string path = string.Empty;
                path = string.Format("{0}", GetRootPath());

                return Path.Combine(GetRootPath(), paths[0]);
            }
            else if (paths.Length == 2)
            {
                return Path.Combine(GetRootPath(), paths[0], paths[1]);
            }
            else if (paths.Length == 3)
            {
                return Path.Combine(GetRootPath(), paths[0], paths[1], paths[2]);
            }
            else if (paths.Length > 3)
            {
                throw new Exception("GetRelativePath 目前不支援超過3個目錄參數陣列");
            }
            return GetRootPath();
        }
        public static void SetRootPath(string rootPath)
        {
            Helper.rootPath = rootPath;
        }
        public static string GetRootPath()
        {
            if (rootPath != null)
            {
                return rootPath;
            }
            if (rootPath == null)
            {
                if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "config.json")))
                {
                    rootPath = Directory.GetCurrentDirectory();
                }
            }
            if (rootPath == null)
            {
                rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            return rootPath;
        }
        private static string AES_IV_FOR_PASSWORD = "3UgyD0JqZSn8gIpZ";
        private static string AES_KEY_FOR_PASSWORD = "NfdfV4M0BFQKzqw28pudE5lKZ2F8eVv9";

        public static string EncryptAESForPassword(string text)
        {
            return EncryptAES(text, AES_KEY_FOR_PASSWORD, AES_IV_FOR_PASSWORD);
        }
        public static string DecryptAESForPassword(string text)

        {
            return DecryptAES(text, AES_KEY_FOR_PASSWORD, AES_IV_FOR_PASSWORD);

        }
        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="text"></param>
        /// <param name="aesKey"></param>
        /// <param name="aesIv"></param>
        /// <returns></returns>
        public static string EncryptAES(string text, string aesKey, string aesIv)
        {
            var sourceBytes = Encoding.UTF8.GetBytes(text);
            var aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(aesKey);
            aes.IV = Encoding.UTF8.GetBytes(aesIv);
            var transform = aes.CreateEncryptor();
            return Convert.ToBase64String(transform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length));
        }
        /// <summary>
        /// AES 解密
        /// </summary>
        /// <param name="text"></param>
        /// <param name="aesKey"></param>
        /// <param name="aesIv"></param>
        /// <returns></returns>
        public static string DecryptAES(string text, string aesKey, string aesIv)
        {
            var encryptBytes = Convert.FromBase64String(text);
            var aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(aesKey);
            aes.IV = Encoding.UTF8.GetBytes(aesIv);
            var transform = aes.CreateDecryptor();
            return Encoding.UTF8.GetString(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
        }
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi == null) return string.Empty;

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string GetClientIp(bool forwardIpFirst = true)
        {
            var context = WebContext.Current;
            string clientIp;
            StringValues strValues;
            if (forwardIpFirst && WebContext.Current.Request.Headers.TryGetValue("X-Forwarded-For", out strValues))
            {
                clientIp = strValues.FirstOrDefault() ?? string.Empty;
                if (clientIp.Contains(','))
                {
                    clientIp = clientIp.Split(',')[0].Trim();
                }
            }
            else
            {
                clientIp = context.Connection.RemoteIpAddress.MapToIPv4().ToString();
            }
            return clientIp;
        }

        public static string ConvertJObject2String(object obj)
        {
            string apiParams = obj.ToString().Replace("ValueKind = Array : ", string.Empty).Replace("ValueKind = Object : ", string.Empty).TrimStart().TrimEnd();
            if (apiParams.StartsWith("\""))
            {
                apiParams = apiParams.Substring(1, apiParams.Length);
            }
            if (apiParams.EndsWith("\""))
            {
                apiParams = apiParams.Substring(0, apiParams.Length - 1);
            }
            return apiParams;
        }
        public static bool IsFlagSet(long value, long mask)
        {
            return ((value & mask) > 0);
        }
        public static bool IsFlagSet(long value, Enum mask)
        {
            return IsFlagSet(Convert.ToInt64(value), Convert.ToInt64(mask));
        }
        public static long SetFlag(bool mark, long value, long mask)
        {
            if (mark)
                return value | mask;
            else
                return value & ~mask;
        }

        public static int GetTimeStamp()
        {
            return ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        }

        public static string GetSha256Encode(string data)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();//建立一個SHA256
            byte[] source = Encoding.UTF8.GetBytes(data);//將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source);//進行SHA256加密

            string result = string.Empty;
            foreach (byte x in crypto)
            {
                result += string.Format("{0:x2}", x);
            }
            return result.ToUpper();
        }

        public static string GetHexSign(long s1, string password)
        {
            string reverseNum = string.Empty;

            for (int i = s1.ToString().Length - 1; i >= 0; i--)
            {
                reverseNum += s1.ToString().Substring(i, 1);
            }

            string num2 = s1.ToString() + reverseNum;
            int sum = 0;
            for (int i = 0; i < num2.Length; i++)
            {
                sum += Convert.ToInt16(num2.Substring(i, 1));
            }
            int n1 = sum % 10;
            string n3 = password + s1.ToString() + reverseNum + n1.ToString();
            return GetSha256Encode(n3);
        }

        public static string RandomNumbers(int n)
        {
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            string rRnds = "";
            while (n > 0)
            {
                rRnds += Enumerable.Range(0, 9)
                            .OrderBy(d => rand.Next())
                            .Take(n).Select(d => d.ToString())
                            .Aggregate(
                                (Accumulate, intValue) => Accumulate + intValue
                            );

                n -=9;
            }
            return rRnds;
        }
        public static string RandomEngNumbers(int n)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789abcdefghijkmnopqrstuvwxyz!@$?_-%^*+";
            int passwordLength = n;
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            string rRnds = new string(chars);
            return rRnds;
        }
    }
}
