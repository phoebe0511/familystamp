﻿using log4net;
using Quartz;
using System.Threading.Tasks;

namespace FamilyStamp.Core.Jobs
{
    [DisallowConcurrentExecution]
    public class DummyJob : IJob
    {
        ILog logger = LogManager.GetLogger(typeof(DummyJob));
        public Task Execute(IJobExecutionContext context)
        {
            logger.Info("DummyJob trigger");


            Task.Delay(60000).Wait();
            return Task.CompletedTask;
        }
    }
}
