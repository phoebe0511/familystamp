﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace FamilyStamp.Core.DbContenxts
{
    public class SqlHelper
    {
        public static object GetDbSchemaVersion()
        {
            using (var dbx = new CommonDbContext())
            {
                using (var cmd = dbx.Database.GetDbConnection().CreateCommand())
                {
                    cmd.CommandText = @"
SELECT ISNULL(value,'') FROM fn_listextendedproperty(@name, default, default, default, default, default, default)";
                    cmd.Parameters.Add(new SqlParameter("@name", "Version"));
                    dbx.Database.OpenConnection();
                    return cmd.ExecuteScalar();
                }
            }
        }
        public static object UpdateDbSchemaVersion(string version)
        {
            using (var dbx = new CommonDbContext())
            {
                using (var cmd = dbx.Database.GetDbConnection().CreateCommand())
                {
                    string sql = string.Format(@"exec sp_updateextendedproperty '{0}', '{1}'", "Version", version);
                    return dbx.Database.ExecuteSqlRaw(sql);                    
                }
            }
        }
    }
}
