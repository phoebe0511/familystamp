﻿using FamilyStamp.Core.Kernel;
using FamilyStamp.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace FamilyStamp.Core.DbContenxts
{
    public class CommonDbContext : DbContext
    {
        private static ILoggerFactory loggerFactory;

        static CommonDbContext()
        {
            loggerFactory = new EntityFrameworkSqlLoggerFactory();
            loggerFactory.AddProvider(new EntityFrameworkSqlLoggerProvider());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connStr = Ioc.GetConfig().connectionString;
            optionsBuilder.UseSqlServer(connStr);

            optionsBuilder.UseLoggerFactory(loggerFactory);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<GiftImage>().HasKey(table => new { table.StampActivityGiftId, table.ImageUrl });
            modelBuilder.Entity<MemberGroupRole>().HasKey(table => new { table.PageId, table.GroupId });
        }

        public DbSet<BackstageAccount> BackstageAccountSet { get; set; }
        public DbSet<ExchangeGiftOrder> ExchangeGiftOrderSet { get; set; }
        public DbSet<GiftCondition> GiftConditionSet { get; set; }
        public DbSet<GiftImage> GiftImageSet { get; set; }
        public DbSet<MemberToken> MemberTokenSet { get; set; }
        public DbSet<StampActivity> StampActivitySet { get; set; }
        public DbSet<StampActivityGift> StampActivityGiftSet { get; set; }
        public DbSet<UserStampBag> UserStampBagSet { get; set; }
        public DbSet<UserStampHistory> UserStampHistorySet { get; set; }
        public DbSet<FamilyGiftPincode> FamilyGiftPincodeSet { get; set; }
        public DbSet<MenuFunction> MenuFunctionSet { get; set; }
        public DbSet<MenuFunctionSection> MenuFunctionSectionSet { get; set; }
        public DbSet<MemberGroupRole> MemberGroupRoleSet { get; set; }
    }
}
