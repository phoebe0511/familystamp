﻿using System;
using System.Runtime.Serialization;

namespace FamilyStamp.Core.Exceptions
{
    public class FamilyStampApiException : Exception, ISerializable
    {
        public FamilyStampApiException() : base("發生異常")
        {
        }
        public FamilyStampApiException(string message) : base(message)
        {

        }
        public FamilyStampApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
