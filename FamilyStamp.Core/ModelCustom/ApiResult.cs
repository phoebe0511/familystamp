﻿using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyStamp.Core.ModelCustom.API
{
    public class ApiResult : ApiResult<object>
    {
    }

    public class ApiResult<T> where T : new()
    {
        private string _message;
        /// <summary>
        /// 回傳的結果
        /// </summary>
        public ApiResultCode Code { get; set; }
        /// <summary>
        /// 回傳資料主體
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 回傳的訊息
        /// </summary>
        public string Message
        {
            get
            {
                return string.IsNullOrEmpty(_message) ? Helper.GetEnumDescription(Code) : _message;
            }
            set
            {
                _message = value;
            }
        }

        public ApiResult()
        {
            Code = ApiResultCode.Fail;
            Data = default(T);
            Message = string.Empty;
        }
    }
}
