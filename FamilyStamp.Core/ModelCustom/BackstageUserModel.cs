﻿using FamilyStamp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyStamp.Core.ModelCustom
{
    public class BackstageUserModel
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string Memo { get; set; }
        public string Name { get; set; }
        public BackstageMemberRole Role { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsValide { get; set; }
    }
}
