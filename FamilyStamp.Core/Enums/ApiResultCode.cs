﻿using System.ComponentModel;

namespace FamilyStamp.Core.Enums
{
    public enum ApiResultCode
    {
        /// <summary>
        /// 失敗
        /// </summary>
        [Description("未明確定義之錯誤")]
        Fail = 0,
        [Description("密碼錯誤")]
        PasswordError = 201,
        /// <summary>
        /// 裝置已被登出
        /// </summary>
        DeviceIsLogout = 403,
        /// <summary>
        /// 會員狀態須被登出
        /// </summary>
        MemberStatusRefuseLogin = 401,
        /// <summary>
        /// 會員狀態須被登出
        /// </summary>
        TokenError = 410,
        //[Description("初始化交易")]
        //Ready = 0,
        [Description("交易成功")]
        Success = 1,
        [Description("未明確定義之錯誤")]
        Error = 1010,
        [Description("輸入參數錯誤")]
        InputError = 1020,
        [Description("憑證無效")]
        TokenInvalid = 2010,
        [Description("憑證已過期")]
        TokenExpired = 2012,
        [Description("使用者未登入或無權限")]
        UserNoSignIn = 2015,
        [Description("查無資料")]
        DataNotFound = 3010,
        [Description("核銷失敗")]
        CouponVerifiedFail = 3020,
    }
}
