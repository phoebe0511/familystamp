﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyStamp.Core.Enums
{
    /// <summary>
    /// 這
    /// </summary>
    public enum MenuItem
    {
        Master = 1000,
        AccountList = 11,
        AccountUpdate = 12,
        ActivityList = 13,
        ActivityUpdate = 14,
        ActivityUpdateGift = 15,
        ActivityGiftList = 16,
        ActivityExchangedHistory = 17,
        MemberStampList = 18
    }
}
