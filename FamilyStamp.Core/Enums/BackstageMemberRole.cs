﻿using System.ComponentModel;


namespace FamilyStamp.Core.Enums
{
    public enum BackstageMemberRole
    {
        [Description("站台管理者(只17使用)")]
        Master = 9100,
        [Description("總管理員")]
        Administrator = 8100,
        [Description("行銷人員")]
        Marketing = 7100,
        [Description("客服人員")]
        CustomerService = 6100
    }
}
