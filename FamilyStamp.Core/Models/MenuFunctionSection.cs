using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("menu_function_section")]
	public class MenuFunctionSection
	{
		public MenuFunctionSection()
		{
			Name = string.Empty;
		}

		[Column("id")]
		public Guid Id { get; set; }

		[Column("name")]
		public string Name { get; set; }

		[Column("seq")]
		public int Seq { get; set; }

		[Column("admin_only")]
		public bool AdminOnly { get; set; }

		[Column("visible")]
		public bool Visible { get; set; }
	}
}
