using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("stamp_activity_gift")]
	public class StampActivityGift
	{
		public StampActivityGift()
		{
			Name = string.Empty;
			Description = string.Empty;
			ModifyUser = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("name")]
		public string Name { get; set; }

		[Column("description")]
		public string Description { get; set; }

		[Column("start_time")]
		public DateTime StartTime { get; set; }

		[Column("end_time")]
		public DateTime EndTime { get; set; }

		[Column("stamp_activity_id")]
		public int StampActivityId { get; set; }

		[Column("gift_type")]
		public int GiftType { get; set; }

		[Column("gift_total_limit")]
		public int GiftTotalLimit { get; set; }

		[Column("user_limit")]
		public int UserLimit { get; set; }

		[Column("gift_exchanged_count")]
		public int GiftExchangedCount { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }

		[Column("modify_user")]
		public string ModifyUser { get; set; }
	}
}
