using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("stamp_activity")]
	public class StampActivity
	{
		public StampActivity()
		{
			Name = string.Empty;
			Description = string.Empty;
			ActivityImage = string.Empty;
			StampImage = string.Empty;
			FamilyActivityId = string.Empty;
			Note = string.Empty;
			ModifyUser = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("name")]
		public string Name { get; set; }

		[Column("description")]
		public string Description { get; set; }

		[Column("start_time")]
		public DateTime StartTime { get; set; }

		[Column("end_time")]
		public DateTime EndTime { get; set; }

		[Column("activity_image")]
		public string ActivityImage { get; set; }

		[Column("stamp_image")]
		public string StampImage { get; set; }

		[Column("family_activity_id", TypeName = "varchar(100)")]
		public string FamilyActivityId { get; set; }

		[Column("note")]
		public string Note { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("modify_user")]
		public string ModifyUser { get; set; }
	}
}
