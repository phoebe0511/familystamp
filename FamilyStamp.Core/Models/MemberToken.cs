using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("member_token")]
	public class MemberToken
	{
		public MemberToken()
		{
			MemberId = string.Empty;
		}

		[Key]
		[Column("member_id")]
		public string MemberId { get; set; }

		[Column("access_token")]
		public Guid AccessToken { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("expired_time")]
		public DateTime ExpiredTime { get; set; }

		[Column("is_never_expired")]
		public bool IsNeverExpired { get; set; }

		[Column("is_valide")]
		public bool IsValide { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}
