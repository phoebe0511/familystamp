using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("gift_image")]
	public class GiftImage
	{
		public GiftImage()
		{
			ImageUrl = string.Empty;
		}

		[Key]
		[Column("stamp_activity_gift_id")]
		public int StampActivityGiftId { get; set; }

		[Key]
		[Column("image_url")]
		public string ImageUrl { get; set; }
	}
}
