using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("user_stamp_bag")]
	public class UserStampBag
	{
		public UserStampBag()
		{
		}

		[Key]
		[Column("id")]
		public long Id { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("stamp_activity_id")]
		public int StampActivityId { get; set; }

		[Column("stamp_count")]
		public int StampCount { get; set; }

		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }
	}
}
