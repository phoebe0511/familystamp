using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("menu_function")]
	public class MenuFunction
	{
		public MenuFunction()
		{
			Name = string.Empty;
			Link = string.Empty;
		}

		[Key]
		[Column("page_id")]
		public int PageId { get; set; }

		[Column("name")]
		public string Name { get; set; }

		[Column("section_id")]
		public Guid SectionId { get; set; }

		[Column("link", TypeName = "varchar(50)")]
		public string Link { get; set; }

		[Column("seq")]
		public int Seq { get; set; }

		[Column("visible")]
		public bool Visible { get; set; }
	}
}
