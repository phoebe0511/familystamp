using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("backstage_account")]
	public class BackstageAccount
	{
		public BackstageAccount()
		{
			Account = string.Empty;
			Name = string.Empty;
			Password = string.Empty;
		}

		[Key]
		[Column("guid")]
		public Guid Guid { get; set; }

		[Column("account")]
		public string Account { get; set; }

		[Column("name")]
		public string Name { get; set; }

		[Column("password")]
		public string Password { get; set; }

		[Column("memo")]
		public string Memo { get; set; }

		[Column("is_valide")]
		public bool IsValide { get; set; }

		[Column("role_id")]
		public int RoleId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("latest_login_time")]
		public DateTime? LatestLoginTime { get; set; }
	}
}
