using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("member_group_role")]
	public class MemberGroupRole
	{
		public MemberGroupRole()
		{
		}

		[Key]
		[Column("group_id")]
		public int GroupId { get; set; }

		[Key]
		[Column("page_id")]
		public int PageId { get; set; }
	}
}
