using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("gift_condition")]
	public class GiftCondition
	{
		public GiftCondition()
		{
			ModifyUser = string.Empty;
		}

		[Column("id")]
		public int Id { get; set; }

		[Column("stamp_activity_gift_id")]
		public int StampActivityGiftId { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("stamp_count")]
		public int StampCount { get; set; }

		[Column("money")]
		public int Money { get; set; }

		[Column("exchanged_gift_qty")]
		public int ExchangedGiftQty { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_time")]
		public DateTime ModifyTime { get; set; }

		[Column("modify_user")]
		public string ModifyUser { get; set; }
	}
}
