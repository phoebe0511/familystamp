using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("family_gift_pincode")]
	public class FamilyGiftPincode
	{
		public FamilyGiftPincode()
		{
			Pincode = string.Empty;
		}

		[Column("id")]
		public long Id { get; set; }

		[Column("gift_condition_id")]
		public int GiftConditionId { get; set; }

		[Column("is_exchanged")]
		public bool IsExchanged { get; set; }

		[Column("pincode", TypeName = "varchar(20)")]
		public string Pincode { get; set; }
	}
}
