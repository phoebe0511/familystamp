using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("sysdiagrams")]
	public class Sysdiagram
	{
		public Sysdiagram()
		{
			Name = string.Empty;
		}

		[Column("name")]
		public string Name { get; set; }

		[Column("principal_id")]
		public int PrincipalId { get; set; }

		[Key]
		[Column("diagram_id")]
		public int DiagramId { get; set; }

		[Column("version")]
		public int? Version { get; set; }

		[Column("definition")]
		public byte[] Definition { get; set; }
	}
}
