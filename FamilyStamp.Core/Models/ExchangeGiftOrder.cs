using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("exchange_gift_order")]
	public class ExchangeGiftOrder
	{
		public ExchangeGiftOrder()
		{
		}

		[Column("id")]
		public long Id { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("stamp_activity_id")]
		public int StampActivityId { get; set; }

		[Column("stamp_activity_gift_id")]
		public int StampActivityGiftId { get; set; }

		[Column("gift_condition_id")]
		public int GiftConditionId { get; set; }

		[Column("stamp_count")]
		public int StampCount { get; set; }

		[Column("gift_qty")]
		public int GiftQty { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}
