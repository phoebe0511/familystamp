using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyStamp.Core.Models
{
	[Table("user_stamp_history")]
	public class UserStampHistory
	{
		public UserStampHistory()
		{
			FamilyStoreName = string.Empty;
		}

		[Key]
		[Column("id")]
		public long Id { get; set; }

		[Column("user_stamp_bag_id")]
		public long UserStampBagId { get; set; }

		[Column("stamp_count")]
		public int StampCount { get; set; }

		[Column("family_store_name")]
		public string FamilyStoreName { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}
