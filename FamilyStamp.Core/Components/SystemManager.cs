﻿using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Kernel;
using FamilyStamp.Core.Models;
using FamilyStamp.Core.Providers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FamilyStamp.Core.Components
{
    public interface ISystemManager
    {
        List<MenuFunctionView> GetSystemFunctions(int roleId);
    }
    public class SystemManager : ISystemManager
    {
        private IBackstageUserProvider bsp = Ioc.Get<IBackstageUserProvider>();
        private List<MenuFunctionSection> _funcSections = new List<MenuFunctionSection>();
        private List<MenuFunction> _funcs = new List<MenuFunction>();

        public SystemManager()
        {
            try
            {
                Reload();
            }
            catch (Exception ex)
            {
                throw new Exception("載入資料失敗，請確認資料庫狀態，並重啟Web服務", ex);
            }
        }
        public void Reload()
        {
            _funcSections = bsp.GetMenuFunctionSections();
            _funcs = bsp.GetMenuFunctions();
        }

        public List<MenuFunctionView> GetSystemFunctions(int roleId)
        {
            List<MenuFunctionView> result = new List<MenuFunctionView>();
            List<int> functionList = bsp.GetMemberRoles(roleId);
            BackstageMemberRole role = (BackstageMemberRole)roleId;
            foreach (var section in _funcSections)
            {
                var sectionView = new MenuFunctionView
                {
                    Id = section.Id,
                    Name = section.Name,
                    Seq = section.Seq,
                    Functions = _funcs.Where(t => t.SectionId == section.Id && (role >= BackstageMemberRole.Administrator || (functionList.Contains(t.PageId)))).OrderBy(x => x.Seq).ToList()
                };
                if (sectionView.Functions.Count == 0)
                {
                    continue;
                }
                result.Add(sectionView);
            }
            return result;
        }
    }
}
