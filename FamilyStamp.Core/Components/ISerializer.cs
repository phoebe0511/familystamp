﻿using System;

namespace FamilyStamp.Core.Components
{
    public interface ISerializer
    {
        string Serialize(object input, bool indented = false);
        T Deserialize<T>(string input);
        object Deserialize(string input, Type objType);
        dynamic DeserializeDynamic(string input);
        bool TryDeserialize<T>(string input, out T t);
    }
}
