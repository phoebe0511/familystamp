﻿using System.ComponentModel;

namespace FamilyStamp.Core.Components.Jobs
{
    public enum JobStatus
    {
        [Description("停用")]
        Disabled = 0,
        [Description("等待中")]
        Queueing = 1,
        [Description("進行中")]
        Running = 2,
        [Description("暫停")]
        Paused = 3
    }
}
