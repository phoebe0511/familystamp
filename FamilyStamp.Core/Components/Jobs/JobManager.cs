﻿using FamilyStamp.Core;
using FamilyStamp.Core.Utilities;
using log4net;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.IO;

namespace FamilyStamp.Core.Components.Jobs
{
    public class JobManager
    {
        private static JobManager _instance = new JobManager();
        private static ILog logger = LogManager.GetLogger(typeof(JobManager));
        public const string _DEFAULT_GROUP = "DEF";

        public static JobManager Current
        {
            get { return _instance; }
        }

        private JobManager()
        {

        }

        private IScheduler scheduler;

        public void Start()
        {
            if (scheduler != null && scheduler.IsStarted)
            {
                return;
            }

            // 建立排程器
            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.GetScheduler().Result;
            scheduler.ListenerManager.AddJobListener(new QuartzJobListener("Default"));

            var jobConfigs = new List<JobConfig>();
            try
            {
                string configPath = Helper.MapPath("jobs.json");
                jobConfigs.AddRange(new JsonSerializer().Deserialize<JobConfig[]>(File.ReadAllText(configPath)));
            }
            catch (Exception ex)
            {
                logger.Warn("Job設定有誤", ex);
            }
            foreach (var jobConfig in jobConfigs)
            {
                try
                {
                    Type jobType = Type.GetType(jobConfig.type);
                    string jobId = RegisterCronJob(jobType, jobConfig.schedule);
                    if (QuartzJobLog.Current.Items.ContainsKey(jobConfig.name))
                    {
                        logger.WarnFormat("jobConfig.name {0} 設定重複", jobConfig.name);
                        continue;
                    }
                    DateTimeOffset? nextRunTime = new CronExpression(jobConfig.schedule).GetNextValidTimeAfter(
                        new DateTimeOffset(DateTime.Now));

                    QuartzJobLog.Current.Items.TryAdd(jobId,
                        new QuartzJobLog.JobItem
                        {
                            Name = jobConfig.name,
                            JobType = jobType,
                            NextRun = nextRunTime.Value.DateTime.ToLocalTime(),
                            Status = JobStatus.Queueing
                        });
                }
                catch (Exception ex)
                {
                    if (jobConfig != null && jobConfig.name != null)
                    {
                        logger.WarnFormat("Job設定有誤，請檢查 {0}. ex={1}", jobConfig.name, ex);
                    }
                    else
                    {
                        logger.Warn("Job設定有誤", ex);
                    }
                }
            }


            // 啟動排程器
            scheduler.Start();
        }

        public void FireJob(string id)
        {
            var jobKey = new JobKey(id, _DEFAULT_GROUP);
            QuartzJobLog.Current.UpdateForBeginRun(id);
            scheduler.TriggerJob(jobKey);
        }

        public string RegisterCronJob(Type type, string cronSchedule, params KeyValuePair<string, object>[] dataMaps)
        {
            string id = type.Name + "::" + Guid.NewGuid().ToString();
            var trigger = TriggerBuilder.Create()
                .WithIdentity("trigger-" + id, _DEFAULT_GROUP)
                .WithCronSchedule(cronSchedule)
                .StartNow();


            IJobDetail job = JobBuilder.Create(type)
                //.WithIdentity(type.Name, _DEFAULT_GROUP)
                //.WithIdentity("job-" + id, "group-" + id)
                .WithIdentity("job-" + id, _DEFAULT_GROUP)
                .Build();

            if (dataMaps != null)
            {
                foreach (var dataMap in dataMaps)
                {
                    job.JobDataMap.Add(dataMap);
                }
            }
            scheduler.ScheduleJob(job, trigger.Build());
            return job.Key.Name;
        }

        public void Stop()
        {
            if (scheduler == null || scheduler.IsStarted == false)
            {
                return;
            }
            scheduler.Shutdown();
        }

    }
}
