﻿using Quartz;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FamilyStamp.Core.Components.Jobs
{
    public class QuartzJobListener : IJobListener
    {
        public string Name { get; set; }
        public QuartzJobListener(string name)
        {
            this.Name = name;
        }

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            string jobId = ((Quartz.Impl.JobDetailImpl)((Quartz.Impl.JobExecutionContextImpl)context).JobDetail).Name;

            QuartzJobLog.JobItem item;
            if (QuartzJobLog.Current.Items.TryGetValue(jobId, out item))
            {
                QuartzJobLog.Current.Items.TryUpdate(jobId, new QuartzJobLog.JobItem
                {
                    Name = item.Name,
                    JobType = item.JobType,
                    LastRun = DateTime.Now,
                    NextRun = item.NextRun,
                    Status = JobStatus.Running
                }, item);
            }

            return Task.CompletedTask;
        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default)
        {
            string jobId = ((Quartz.Impl.JobDetailImpl)((Quartz.Impl.JobExecutionContextImpl)context).JobDetail).Name;

            QuartzJobLog.JobItem item;
            if (QuartzJobLog.Current.Items.TryGetValue(jobId, out item))
            {
                DateTime? nextTime;
                if (context.NextFireTimeUtc == null)
                {
                    nextTime = item.NextRun;
                }
                else
                {
                    nextTime = context.NextFireTimeUtc.Value.LocalDateTime;
                }
                QuartzJobLog.Current.Items.TryUpdate(jobId, new QuartzJobLog.JobItem
                {
                    Name = item.Name,
                    JobType = item.JobType,
                    LastRun = item.LastRun,
                    NextRun = nextTime,
                    Status = JobStatus.Queueing,
                    LastExecution = item.LastRun == null ? TimeSpan.Zero : DateTime.Now - item.LastRun.Value
                }, item);
            }

            return Task.CompletedTask;
        }
    }
}
