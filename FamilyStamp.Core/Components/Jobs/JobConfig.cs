﻿namespace FamilyStamp.Core.Components.Jobs
{
    public class JobConfig
    {
        public string name { get; set; }
        public string type { get; set; }
        public string schedule { get; set; }
    }
}
