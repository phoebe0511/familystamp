﻿using System;
using System.Collections.Concurrent;

namespace FamilyStamp.Core.Components.Jobs
{
    public class QuartzJobLog
    {
        private static QuartzJobLog _instance = new QuartzJobLog();
        public static QuartzJobLog Current
        {
            get
            {
                return _instance;
            }
        }
        private QuartzJobLog()
        {
            Items = new ConcurrentDictionary<string, JobItem>();
        }
        public ConcurrentDictionary<string, JobItem> Items { get; private set; }


        public class JobItem
        {
            public string Name { get; set; }
            public Type JobType { get; set; }
            public DateTime? LastRun { get; set; }
            public DateTime? NextRun { get; set; }
            public TimeSpan? LastExecution { get; set; }
            public JobStatus Status { get; set; }
        }

        public void UpdateForBeginRun(string id)
        {
            JobItem item;
            if (Items.TryGetValue(id, out item))
            {
                Items.TryUpdate(id, new JobItem
                {
                    Name = item.Name,
                    JobType = item.JobType,
                    LastRun = DateTime.Now,
                    NextRun = item.NextRun,
                    Status = JobStatus.Running
                }, item);
            }
        }
    }
}
