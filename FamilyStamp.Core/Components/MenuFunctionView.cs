﻿
using FamilyStamp.Core.Models;
using System.Collections.Generic;

namespace FamilyStamp.Core.Components
{
    public class MenuFunctionView : MenuFunctionSection
    {
        public MenuFunctionView()
        {
            this.Functions = new List<MenuFunction>();
        }
        public List<MenuFunction> Functions { get; set; }
        
    }
    public class SystemVars
    {
        public const int _TOKEN_EXPIRED_SECS = 600;
        public const string _TOKEN_TYPE = "Bearer";
    }
}
