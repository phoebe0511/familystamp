﻿using System;

namespace FamilyStamp.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyBranchDatabaseSchemaAttribute : Attribute
    {
        private int _schemaVersion;
        public int SchemaVersion
        {
            get { return _schemaVersion; }
        }

        public AssemblyBranchDatabaseSchemaAttribute(int schVer)
        {
            _schemaVersion = schVer;
        }
    }
}
