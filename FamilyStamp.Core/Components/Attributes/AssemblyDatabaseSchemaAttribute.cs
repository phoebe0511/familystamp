﻿using System;

namespace FamilyStamp.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyDatabaseSchemaAttribute : Attribute
    {
        private string _schemaVersion;
        public string SchemaVersion
        {
            get { return _schemaVersion; }
        }

        public AssemblyDatabaseSchemaAttribute(string schVer)
        {
            _schemaVersion = schVer;
        }
    }
}
