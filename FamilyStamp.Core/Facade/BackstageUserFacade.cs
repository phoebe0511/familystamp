﻿using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Kernel;
using FamilyStamp.Core.ModelCustom;
using FamilyStamp.Core.Models;
using FamilyStamp.Core.Providers;
using FamilyStamp.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyStamp.Core.Facade
{
    public class BackstageUserFacade
    {
        private static IBackstageUserProvider _bup = Ioc.Get<IBackstageUserProvider>();
        public static bool VerifyUser(string account, string password, out BackstageUserModel user)
        {
            if (string.IsNullOrEmpty(account) || string.IsNullOrEmpty(password))
            {
                user = null;
                return false;
            }
            var dbUser = _bup.GetUser(account);
            if(dbUser == null || dbUser.Password != Helper.GetSha256Encode(password) || !dbUser.IsValide)
            {
                user = new BackstageUserModel();
                return false;
            }
            user = new BackstageUserModel
            {
                Account = dbUser.Account,
                Name = dbUser.Name,
                Memo = dbUser.Memo,
                Role = (BackstageMemberRole)dbUser.RoleId,
                CreateTime = dbUser.CreateTime
            };
            dbUser.LatestLoginTime = DateTime.Now;
            _bup.AddOrUpdateUser(dbUser);
            return true;
        }

        public static bool AddOrUpdateUser(BackstageUserModel user)
        {
            if(string.IsNullOrEmpty(user.Account))
            {
                return false;
            }

            var dbUser = _bup.GetUser(user.Account);
            if(dbUser == null)
            {
                if(string.IsNullOrEmpty(user.Account) || string.IsNullOrEmpty(user.Password))
                {
                    return false;
                }
                dbUser = new BackstageAccount
                {
                    Guid = Guid.NewGuid(),
                    Account = user.Account,
                    Password = Helper.GetSha256Encode(user.Password),
                    Name = user.Name ?? string.Empty,
                    Memo = user.Memo,
                    RoleId = (int)user.Role,
                    CreateTime = DateTime.Now
                };
            }
            else
            {
                dbUser.Name = user.Name;
                dbUser.Memo = user.Memo;
                dbUser.RoleId = (int)user.Role;
                dbUser.ModifyTime = DateTime.Now;
                dbUser.IsValide = user.IsValide;
                if (!string.IsNullOrEmpty(user.Password))
                {
                    dbUser.Password = Helper.GetSha256Encode(user.Password);
                }
            }
            return _bup.AddOrUpdateUser(dbUser);
        }
    }
}
