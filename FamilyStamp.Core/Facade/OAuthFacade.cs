﻿using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Kernel;
using FamilyStamp.Core.Providers;
using log4net;
using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyStamp.Core.Facade
{
    class OAuthFacade
    {
        private static ILog _log = LogManager.GetLogger(typeof(OAuthFacade));
        private static IOAuthProvider _oap = Ioc.Get<IOAuthProvider>();

        public static bool AddIfNewFamilyMember(string memberId)
        {
            var token = _oap.GetMemberToken(memberId);
            if (token.AccessToken == null || token.AccessToken == Guid.Empty)
            {
                return _oap.AddMemberToken(new Models.MemberToken
                {
                    MemberId = memberId,
                    IsValide = true,
                    IsNeverExpired = false,
                    AccessToken = Guid.NewGuid(),
                    ExpiredTime = DateTime.Now.AddHours(24),
                    CreateTime = DateTime.Now
                });
            }
            return true;
        }

        public static Guid GetAccessToken(string memberId, out ApiResultCode resultCode)
        {
            var token = _oap.GetMemberToken(memberId);
            if (token.AccessToken == null || token.AccessToken == Guid.Empty)
            {
                resultCode = ApiResultCode.DataNotFound;
            }
            else
            {
                if (token.IsValide)
                {
                    resultCode = ApiResultCode.Success;
                    if (!token.IsNeverExpired)
                    {
                        if (token.ExpiredTime < DateTime.Now)
                        {
                            //access token過期時renew
                            token.AccessToken = Guid.NewGuid();
                            token.ExpiredTime = DateTime.Now.AddHours(24);
                            _oap.UpdateMemberToken(token);
                        }
                    }
                    return token.AccessToken;
                }
                else
                {
                    resultCode = ApiResultCode.TokenInvalid;
                }
            }
            return Guid.Empty;
        }

        public static ApiResultCode CheckAccessToken(Guid accessToken)
        {
            ApiResultCode resultCode;
            var token = _oap.GetMemberTokenByGuid(accessToken);
            if (token.AccessToken == null || token.AccessToken == Guid.Empty)
            {
                resultCode = ApiResultCode.TokenError;
            }
            else
            {
                if (token.IsValide)
                {
                    resultCode = ApiResultCode.Success;
                    if (!token.IsNeverExpired)
                    {
                        if (token.ExpiredTime < DateTime.Now)
                        {
                            //access token過期
                            resultCode = ApiResultCode.TokenExpired;
                        }
                    }
                }
                else
                {
                    resultCode = ApiResultCode.TokenInvalid;
                }
            }
            return resultCode;
        }
    }
}
