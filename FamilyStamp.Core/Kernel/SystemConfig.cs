﻿using FamilyStamp.Core.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.Json;

namespace FamilyStamp.Core.Kernel
{
    public interface ISystemConfig
    {
        string connectionString { get; set; }
        int port { get; set; }
        string workingDirectoryPath { get; set; }
    }
    public class SystemConfig : ISystemConfig
    {
        static ILog logger = LogManager.GetLogger(typeof(SystemConfig));

        public string connectionString { get; set; }
        public int port { get; set; }
        public string workingDirectoryPath { get; set; }

        public static SystemConfig Reload()
        {
            string configFile = Helper.MapPath(string.Format("config.{0}.json", System.Environment.MachineName));
            if (File.Exists(configFile) == false)
            {
                configFile = Helper.MapPath("config.json");
            }
            logger.Warn("載入 " + configFile);
            string json = File.ReadAllText(configFile);
            return JsonSerializer.Deserialize<SystemConfig>(json);
        }
    }
}
