﻿using Autofac;
using FamilyStamp.Core.Components;
using FamilyStamp.Core.Providers;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Reflection;

namespace FamilyStamp.Core.Kernel
{
    public class Ioc
    {
        private static IContainer container;
        public static void Register(string[] args)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SystemManager>()
              .As<ISystemManager>()
              .SingleInstance();

            builder.RegisterInstance<SystemConfig>(SystemConfig.Reload())
                    .As<ISystemConfig>()
                    .SingleInstance();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => typeof(IProvider).IsAssignableFrom(t)).AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterInstance(new MemoryCache(new MemoryCacheOptions()));
            container = builder.Build();

        }
        // for unit test
        public static void Register(ContainerBuilder builder)
        {
            container = builder.Build();
        }
        public static T Get<T>() where T : class
        {
            if (typeof(T).IsInterface == false)
            {
                throw new Exception("T must be interface");
            }
            return container.Resolve<T>();
        }

        public static T Get<T>(string name) where T : class
        {
            if (typeof(T).IsInterface == false)
            {
                throw new Exception("T must be interface");
            }
            return container.ResolveNamed<T>(name);
        }

        public static ISystemConfig GetConfig()
        {
            return Get<ISystemConfig>();
        }

        public static MemoryCache GetCache()
        {
            return container.Resolve<MemoryCache>();
        }
    }
}
