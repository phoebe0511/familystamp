﻿namespace FamilyStamp.Core.Kernel
{
    public class ConstStrings
    {
        public static string _SITE_TITLE = "全家數位戳章";
        public static string _SITE_TITLE_BACKEND
        {
            get
            {
                return string.Format("{0}管理後臺", _SITE_TITLE);
            }
        }
    }
}
