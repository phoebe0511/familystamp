﻿using FamilyStamp.Core.DbContenxts;
using FamilyStamp.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FamilyStamp.Core.Providers
{
    public interface IOAuthProvider: IProvider
    {
        bool AddMemberToken(MemberToken token);
        bool UpdateMemberToken(MemberToken token);
        bool RemoveMemberToken(string memberId);
        MemberToken GetMemberToken(string memberId);
        MemberToken GetMemberTokenByGuid(Guid accessToken);
    }
    class OAuthProvider : IOAuthProvider
    {
        public bool AddMemberToken(MemberToken token)
        {
            using (var dbx = new CommonDbContext())
            {
                dbx.MemberTokenSet.Add(token);
                return dbx.SaveChanges() > 0;
            }
        }

        public MemberToken GetMemberToken(string memberId)
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.MemberTokenSet.Where(x => x.MemberId == memberId).FirstOrDefault();
            }
        }

        public MemberToken GetMemberTokenByGuid(Guid accessToken)
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.MemberTokenSet.Where(x => x.AccessToken == accessToken).FirstOrDefault();
            }
        }

        public bool RemoveMemberToken(string memberId)
        {
            using (var dbx = new CommonDbContext()) 
            {
                dbx.MemberTokenSet.Remove(dbx.MemberTokenSet.Where(x => x.MemberId == memberId).FirstOrDefault());
                return dbx.SaveChanges() > 0;
            }
        }

        public bool UpdateMemberToken(MemberToken token)
        {
            using(var dbx = new CommonDbContext())
            {
                dbx.Entry(token).State = EntityState.Modified;
                return dbx.SaveChanges() > 0;
            }
        }
    }
}
