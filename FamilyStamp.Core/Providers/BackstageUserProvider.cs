﻿using FamilyStamp.Core.DbContenxts;
using FamilyStamp.Core.Enums;
using FamilyStamp.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FamilyStamp.Core.Providers
{
    public interface IBackstageUserProvider : IProvider
    {
        public bool CheckUserRole(string account, string password, out int roleId);
        public bool AddOrUpdateUser(BackstageAccount backstageAccount);
        public List<BackstageAccount> GetUsers(int numOfUsers, int offsetOfUsers);
        public BackstageAccount GetUser(string account);
        List<MenuFunctionSection> GetMenuFunctionSections();
        List<MenuFunction> GetMenuFunctions();
        List<int> GetMemberRoles(int GroupId);
    }
    class BackstageUserProvider : IBackstageUserProvider
    {
        public bool AddOrUpdateUser(BackstageAccount backstageAccount)
        {
            using (var dbx = new CommonDbContext())
            {
                if (backstageAccount.Guid == Guid.Empty)
                {
                    backstageAccount.Guid = Guid.NewGuid();
                    backstageAccount.CreateTime = DateTime.Now;
                    dbx.Entry(backstageAccount).State = EntityState.Added;
                }
                else
                {
                    dbx.Entry(backstageAccount).State = EntityState.Modified;
                }
                return dbx.SaveChanges() > 0;
            }
        }

        public bool CheckUserRole(string name, string password, out int roleId)
        {
            throw new NotImplementedException();
        }

        public BackstageAccount GetUser(string account)
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.BackstageAccountSet.Where(x => x.Account == account).FirstOrDefault();
            }
        }


        public List<BackstageAccount> GetUsers(int numOfUsers, int offsetOfUsers)
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.BackstageAccountSet.OrderByDescending(x => x.CreateTime)
                        .Skip(offsetOfUsers)
                        .Take(numOfUsers)
                        .ToList();
            }
        }

        public List<MenuFunctionSection> GetMenuFunctionSections()
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.MenuFunctionSectionSet.OrderBy(t => t.Seq).ToList();
            }
        }

        public List<MenuFunction> GetMenuFunctions()
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.MenuFunctionSet.OrderBy(t => t.Seq).ToList();
            }
        }

        public List<int> GetMemberRoles(int GroupId)
        {
            using (var dbx = new CommonDbContext())
            {
                return dbx.MemberGroupRoleSet.Where(x => x.GroupId == GroupId).Select(x => x.PageId).ToList();
            }
        }
    }
}
